"use strict";

const MowerParser = require("./lib/MowerParser");
const MowerWorker = require("./lib/MowerWorker");

/**
 * @param {String} content
 * @returns {Object} see {@link Lawnmower#getPosition} to know how is it composed of
 */
function automower (content) {
    let mowerParser = new MowerParser(content);

    return new MowerWorker(mowerParser.lawnmowers);
}

module.exports = automower;
