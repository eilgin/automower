"use strict";

const Point = require("./Point");
const Direction = require("./Direction");

class Lawnmower {
    /**
     * This will initialize the position and direction of the lawnmower
     *
     * @param {Lawn} lawn
     * @param {Point} initialCoordinate
     * @param {String} cardinalDirection
     * @param {Array.<String>} instructions
     */
    constructor (lawn, initialCoordinate, cardinalDirection, instructions) {
        this.lawn = lawn;
        this.coordinate = initialCoordinate;
        this.direction = new Direction(cardinalDirection);
        this.instructions = instructions;
    }

    /**
     * get the position including the cardinal direction of this lawnmower
     *
     * @returns {{x: Number, y: Number, cardinal: String}}
     */
    getPosition () {
        return {
            x: this.coordinate.x,
            y: this.coordinate.y,
            cardinal: this.direction.getCardinalDirection()
        };
    }

    /**
     * retrieve and execute the current instruction
     *
     * @returns {Object} see {@link getPosition}
     */
    * moveIterator () {
        // return the first position before iterating on the instructions
        yield this.getPosition();

        for (let instruction of this.instructions) {
            this.execute(instruction);
            yield this.getPosition();
        }
    }

    /**
     * Executes the specified instruction
     *
     * @param {String} instruction
     * @throws {Error} if it cannot execute the specified instruction
     */
    execute (instruction) {
        switch (instruction) {
            case "D":
                this.goRight();
                break;
            case "G":
                this.goLeft();
                break;
            case "A":
                this.goForward();
                break;
            default:
                throw new Error(`cannot execute this instruction: ${instruction}`);
        }
    }

    goRight () {
        this.direction.turnClockwise();
    }

    goLeft () {
        this.direction.turnCounterClockwise();
    }

    /**
     * Before going forward, we need to test if the lawnmower doesn't go outside the lawn
     */
    goForward () {
        let directionVector = this.direction.getVector();
        let newCoordinate = new Point(
            this.coordinate.x + directionVector.x,
            this.coordinate.y + directionVector.y
        );

        if (this.lawn.contains(newCoordinate)) {
            this.coordinate = newCoordinate;
        }
    }
}

module.exports = Lawnmower;
