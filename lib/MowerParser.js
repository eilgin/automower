"use strict";

const Point = require("./Point");
const Lawn = require("./Lawn");
const Lawnmower = require("./Lawnmower");

class MowerParser {
    /**
     * This will prepare the sequence to move each lawnmowers
     *
     * @param {String} instructionsContent
     * @throws {Error} if there's an error in the content
     */
    constructor (instructionsContent) {
        // we assume that the content is normalized (unix-style line breaks)
        let lines = instructionsContent.split("\n");

        // check if the content looks valid
        let lineNumber = lines.length;

        if (lineNumber < 3 || lineNumber % 2 === 0) {
            throw new Error(`content is invalid since it misses some lines (line number: ${lineNumber})`);
        }

        // the first line is the size of the lawn: ${width} + whitespace + ${height}
        let lawnLine = lines.shift();
        let lawnSize = lawnLine.split(/\s/);

        if (lawnSize.length !== 2) {
            throw new Error(`wrong lawn dimensions found (should be 2, found ${lawnSize.length})`);
        }

        let lawnWidth = lawnSize.shift();
        let lawnHeight = lawnSize.shift();
        let lawn = new Lawn(lawnWidth, lawnHeight);

        // other lines are in pair:
        // * the first part is related to the initial position of one lawnmower:
        //  - ${xPosition} + whitespace + ${yPosition} + whitespace + ${cardinalDirection}
        //  - ${cardinalDirection} can be one of the following character: "N", "E", "W" or "S"
        // * the second is a sequence of characters where each character commands the movement of this lawnmower
        //  - a command is one of the following character: "D", "G" or "A"
        this.lawnmowers = [];

        do {
            let lawnmowerLine = lines.shift();
            let instructionsLine = lines.shift();

            let lawnmowerCoords = lawnmowerLine.split(/\s/);
            let instructions = instructionsLine.split("");

            if (lawnmowerCoords.length !== 3) {
                throw new Error(`wrong lawnmower coordinates found (should be 3, found ${lawnmowerCoords.length})`);
            }

            let xPosition = lawnmowerCoords.shift();
            let yPosition = lawnmowerCoords.shift();
            let cardinalDirection = lawnmowerCoords.shift();

            let coordinate = new Point(xPosition, yPosition);
            let lawnmower = new Lawnmower(lawn, coordinate, cardinalDirection, instructions);
            this.lawnmowers.push(lawnmower);

        } while (lines.length > 0);
    }
}

module.exports = MowerParser;
