"use strict";

class MowerWorker {
    /**
     * @param {Array.<Lawnmower>} lawnmowers
     * @throws {Error} if "lawnmowers" is empty
     */
    constructor (lawnmowers) {
        if (lawnmowers.length === 0) {
            throw new Error("cannot run an empty list of lawnmowers");
        }

        this.lawnmowers = lawnmowers;
        this.currentMowerIndex = -1;
    }

    getCurrentMower () {
        return this.lawnmowers[this.currentMowerIndex];
    }

    hasNext () {
        return (this.currentMowerIndex + 1) < this.lawnmowers.length;
    }

    /**
     * Executes one movement at a time for the current lawnmower
     *
     * @returns {Object} see {@link Lawnmower#getPosition}
     */
    * run () {
        // advances the iterator cursor
        this.currentMowerIndex++;

        yield* this.getCurrentMower().moveIterator();
    }
}

module.exports = MowerWorker;
