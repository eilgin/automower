# Automower

This typescript library is a solution to `doc/problem.pdf`.

## Requirements

- install Node via [`nvm`](https://github.com/creationix/nvm) and specify the _LTS_ version (4.2.x)

## Installing third-party packages
-  run `npm install`

## Testing

Run the tests (after compiling) with:
```bash
npm run test -s
```
