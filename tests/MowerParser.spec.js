"use strict";

const fs = require("fs");
const MowerParser = require("../lib/MowerParser");
require("chai").should();

describe("MowerParser", function () {
    let content;

    before(function (done) {
        fs.readFile("tests/fixtures/test_spec.txt", "utf8", function (err, data) {
            if (err) throw err;

            content = data;

            done();
        });
    });

    it("creates successfully", function () {
        let mowerParser = new MowerParser(content);

        mowerParser.lawnmowers.should.be.an("array").of.length(2);
    });

    it("throws an error if there's missing content", function () {
        let missingContent = "5 5\n1 1\n";

        (() => new MowerParser(missingContent)).should.throw(Error);
    });

    it("throws an error if the lawn size is wrong", function () {
        let wrongLawnSizeContent = "5\n1 1 E\nAAA";

        (() => new MowerParser(wrongLawnSizeContent)).should.throw(Error);
    });

    it("throws an error if the lawnmower coordinate is wrong", function () {
        let wrongCoordinateContent = "5 5\n1 1\nAAA";

        (() => new MowerParser(wrongCoordinateContent)).should.throw(Error);
    });
});
