"use strict";

const fs = require("fs");
const MowerWorker = require("../lib/MowerWorker");
const MowerParser = require("../lib/MowerParser");
require("chai").should();

/**
 * @param {{x, y, cardinal}} pos
 * @returns {String} the formatted version of "pos" to match with the expected output
 */
function formatOutput (pos) {
    return `${pos.x} ${pos.y} ${pos.cardinal}`;
}

describe("MowerWorker", function () {
    let lawnmowers;
    let expectedOutputLines;

    before(function (done) {
        fs.readFile("tests/fixtures/test_spec.txt", "utf8", function (err, data) {
            if (err) throw err;

            let mowerParser = new MowerParser(data);
            lawnmowers = mowerParser.lawnmowers;

            done();
        });
    });

    before(function (done) {
        fs.readFile("tests/fixtures/expect_output.txt", "utf8", function (err, data) {
            if (err) throw err;

            // assume that "expect_output" is using unix line-breaks
            expectedOutputLines = data.split("\n");

            done();
        });
    });

    it("creates successfully", function () {
        let mowerWorker = new MowerWorker(lawnmowers);

        mowerWorker.lawnmowers.should.be.an("array").of.length(2);
    });

    it("throws an error if there's no lawnmowers", function () {
        (() => new MowerWorker([])).should.throw(Error);
    });

    it("runs the lawnmowers one by one", function () {
        let mowerWorker = new MowerWorker(lawnmowers);
        let finalMowersPos = [];

        while (mowerWorker.hasNext()) {
            // dummy loop to consume all instructions since we only want the final position
            for (let mowerPos of mowerWorker.run()) {
                mowerPos.should.be.an("object");
            }

            let currentMower = mowerWorker.getCurrentMower();
            finalMowersPos.push(currentMower.getPosition());
        }

        let formattedMowerPos = finalMowersPos.map(pos => formatOutput(pos));

        formattedMowerPos.should.be.an("array").eql(expectedOutputLines);
    });
});
