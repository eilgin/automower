"use strict";

const Lawnmower = require("../lib/Lawnmower");
const Lawn = require("../lib/Lawn");
const Point = require("../lib/Point");
require("chai").should();

describe("Lawnmower", function () {
    let lawn;
    let initPos;
    let initDir;
    let instructions;
    let lawnmower;

    before(function () {
        lawn = new Lawn(5, 5);
        initPos = new Point(3, 2);
        initDir = "N";
        instructions = ["A", "D", "A"];
    });

    beforeEach(function () {
        lawnmower = new Lawnmower(lawn, initPos, initDir, instructions);
    });

    it("goes right", function () {
        lawnmower.goRight();

        lawnmower.coordinate.should.be.eql(initPos);
        lawnmower.direction.getCardinalDirection().should.be.equal("E");
    });

    it("goes left", function () {
        lawnmower.goLeft();

        lawnmower.coordinate.should.be.eql(initPos);
        lawnmower.direction.getCardinalDirection().should.be.equal("W");
    });

    it("goes forward", function () {
        let newPos = new Point(3, 3);

        lawnmower.goForward();

        lawnmower.coordinate.should.be.eql(newPos);
        lawnmower.direction.getCardinalDirection().should.be.equal(initDir);
    });

    it("cannot go forward", function () {
        let newCoordinate = new Point(0, 5);
        let expectedPoint = new Point(0, 5);
        // set lawnmover to the upper side facing north
        lawnmower.coordinate = newCoordinate;

        lawnmower.goForward();

        lawnmower.coordinate.should.be.eql(expectedPoint);
        lawnmower.direction.getCardinalDirection().should.be.equal(initDir);
    });

    it("cannot go forward but can still change direction", function () {
        let newCoordinate = new Point(0, 5);
        let expectedPoint = new Point(0, 5);
        // set lawnmover to the upper side facing north
        lawnmower.coordinate = newCoordinate;

        lawnmower.goForward();
        lawnmower.goLeft();

        lawnmower.coordinate.should.be.eql(expectedPoint);
        lawnmower.direction.getCardinalDirection().should.be.equal("W");
    });

    it("executes a 'D' instruction", function () {
        lawnmower.execute("D");

        lawnmower.direction.getCardinalDirection().should.be.equal("E");
    });

    it("throws an error with a 'R' instruction", function () {
        (() => lawnmower.execute("R")).should.throw(Error);
    });

    it("executes all the instructions", function () {
        let expectedFinalPos = {
            x: 4,
            y: 3,
            cardinal: "E"
        };

        // dummy for-of loop to unroll all instructions
        for (let pos of lawnmower.moveIterator()) {
            pos.should.be.an("object");
        }

        lawnmower.getPosition().should.be.eql(expectedFinalPos);
    });
});
