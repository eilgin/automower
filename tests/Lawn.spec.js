"use strict";

const Lawn = require("../lib/Lawn");
const Point = require("../lib/Point");
require("chai").should();

describe("Lawn", function () {
    it("contains the point", function () {
        let lawn = new Lawn(5, 5);
        let point = new Point(5, 2);

        lawn.contains(point).should.be.a("boolean").equal(true);
    });

    it("rejects point outside", function () {
        let lawn = new Lawn(5, 5);
        let point = new Point(-1, 5);

        lawn.contains(point).should.be.a("boolean").equal(false);
    });
});
