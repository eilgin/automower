"use strict";

const Direction = require("../lib/Direction");
const Point = require("../lib/Point");
require("chai").should();

describe("Direction", function () {
    it("creates successfully", function () {
        let direction = new Direction("E");

        direction.getCardinalDirection().should.be.a("string").equal("E");
    });

    it("throws in case of unknown cardinal direction", function () {
        (() => new Direction("O")).should.throw(Error);
    });

    it("goes from north to east when turning clockwise", function () {
        let direction = new Direction("N");

        direction.turnClockwise();

        direction.getCardinalDirection().should.be.a("string").equal("E");
    });

    it("goes from south to east when turning counter-clockwise", function () {
        let direction = new Direction("S");

        direction.turnCounterClockwise();

        direction.getCardinalDirection().should.be.a("string").equal("E");
    });

    it("returns the east vector when it faces east", function () {
        let direction = new Direction("W");
        let expectedVector = new Point(1, 0);

        direction.turnCounterClockwise();
        direction.turnCounterClockwise();

        direction.getVector().should.be.an.instanceof(Point).eql(expectedVector);
    });
});
